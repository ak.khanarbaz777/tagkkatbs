import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { PaymentHistoryComponent } from './pages/payment-history/payment-history.component';
import { WithdrawalComponent } from './pages/withdrawal/withdrawal.component';
import { BanksComponent } from './pages/banks/banks.component';
import { BusinessProfileComponent } from './pages/business-profile/business-profile.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { UserverifyComponent } from './pages/userverify/userverify.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { UserProfile2Component } from './pages/user-profile2/user-profile2.component';
import { ProjectDetailComponent } from './pages/project-detail/project-detail.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    GooglePlaceModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    PaymentHistoryComponent,
    WithdrawalComponent,
    BanksComponent,
    BusinessProfileComponent,
    UserverifyComponent,
    ProjectsComponent,
    UserProfile2Component,
    ProjectDetailComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
