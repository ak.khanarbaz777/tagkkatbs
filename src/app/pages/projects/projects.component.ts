import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentService } from '../dashboard/payment.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(private http: HttpClient,private router:Router,public payment: PaymentService) { }
  projects: any;
  ApiUrl = environment.baseUrl;

  ngOnInit(): void {
    this.ProjectLists();
  }
  ProjectLists() {
    this.http.get(this.ApiUrl+'/projects/project-lists.php').subscribe((result: any) => {
      if (result.error == 0) {
        this.projects = result.projects;
      }
    })

  }
  
  amount: number = 5000;
  amountError: string;
  isDisable: boolean = false;
  
  GetAmountValue(event: any) {
    this.amount = event.target.value;
    if (this.amount >= 5000) {
      this.isDisable = false;
      this.amountError = '';
    } else {
      this.isDisable = true;
      this.amountError = "Amount Must be Greater than 5000";
    }
  }

  rzp1;

  paymentSuccess(data: Object) {
    this.http.post(this.ApiUrl+'/payments/payment-verify.php', data).subscribe((result: any) => {
      if (result.error == 0) {
        this.router.navigate(['/payment-history']);
      } else {
        alert(result.msg);
      }
    });
  }

  generateOrderId(result: any) {
    let options = {
      "key": "rzp_live_JO1dLDBywoLQcm",
      "amount": result.amount,
      "currency": "INR",
      "name": "Invest Now",
      "description": "Invest now to tagkka",
      "image": "https://tagkka.com/tbs.png",
      "order_id": result.order_id,
      "handler": function (response: any) {
        let mydata = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'razorpay_payment_id': response.razorpay_payment_id, 'razorpay_order_id': response.razorpay_order_id, 'razorpay_signature': response.razorpay_signature, 'amount': this.amount };
        this.paymentSuccess(mydata);
      }.bind(this),
      "prefill": {
        "name": result.fullname,
        "email": result.email,
        "contact": result.mobile
      },
      "notes": {
        "address": "Razorpay Corporate Office"
      },
      "theme": {
        "color": "#3399cc"
      }
    };
    return options;

  }

  PayBtn() {
    let amount = this.amount*100;
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'amount': amount };

    this.http.post(this.ApiUrl+'/payments/generate-order.php', param).subscribe((result: Object) => {
      let options = this.generateOrderId(result);
      this.rzp1 = new this.payment.nativeWindow.Razorpay(options);
      this.rzp1.open();
    })


  }

}
