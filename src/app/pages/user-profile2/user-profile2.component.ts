import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-profile2',
  templateUrl: './user-profile2.component.html',
  styleUrls: ['./user-profile2.component.scss']
})
export class UserProfile2Component implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }
  ApiUrl = environment.baseUrl;
  referalData = [];
  TicketsList = [];
  fileToUpload: File | null = null;

  AddNewTicketMsg: string;

  ticket_id = "";
  ticket_title = "";
  ticket_description = "";
  userComments = '';

  noImage: string;


  user_id: any = parseInt(localStorage.getItem('user_id'));
  auth_key: string = localStorage.getItem('auth_key');
  fullname: string;
  wallet: string;
  mobile: string;
  email: string;
  aadhaar_card: string;
  pan_card: string;
  aadhaar_card_image: string;
  pancard_image: string;
  paddress: string;
  pcity: string;
  pstate: string;
  pcode: string;
  caddress: string;
  ccity: string;
  cstate: string;
  ccode: string;
  user_profile: string;

  ErrorMsg: string;
  ToggleEditProfile: string = 'none';
  ToggleShowProfile: string = 'block';
  KycFirstPage: string = 'block';
  documentUploadDiv: string = 'none';
  UploadUserBankDiv: string = 'none';
  CompleteKycDiv: string = 'none';

  total_investment: number;
  mobile_otp: string;
  email_otp: string;
  mobile_main_filed: string;
  email_main_filed: string;
  ErrorMobileNumber: string;
  ErrorEmailNumber: string;
  ErrorOtpMessage: string;
  ErrorEmailOtpMessage: string;
  SendMobileOtp: boolean;
  SendEmailOtp: boolean;

  AgentRequestMessage: string;
  AgentRequestFormDiv: string;
  AgentRequestMessageDiv: string;

  bankdata: Object;
  bankrecords: number;
  ErrorAccountNumber: string;



  AddNewTicket = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    title: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required])

  });

  AddCommentData = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    ticket_id: new FormControl('', [Validators.required]),
    comment: new FormControl('', [Validators.required, Validators.minLength(2)])
  });

  AddBankForm = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    account_name: new FormControl('', [Validators.required, Validators.minLength(4)]),
    bank_name: new FormControl('', [Validators.required, Validators.minLength(4)]),
    account_number: new FormControl('', [Validators.required, Validators.minLength(8)]),
    ifsc_code: new FormControl('', [Validators.required, Validators.minLength(6)]),
    upi: new FormControl('N/A', [Validators.required])

  });

  BecameAnAgent = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    fullname: new FormControl('', [Validators.required, Validators.minLength(2)]),
    occupation: new FormControl('', [Validators.required]),
    additional_mobile_number: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.minLength(5)])
  });



  get GetAccountName() {
    return this.AddBankForm.get('account_name');
  }

  get GetBank_name() {
    return this.AddBankForm.get('bank_name');
  }
  get GetAccount_number() {
    return this.AddBankForm.get('account_number');
  }



  get GetRe_account_number() {
    return this.AddBankForm.get('re_account_number');
  }
  get GetIfsc_code() {
    return this.AddBankForm.get('ifsc_code');
  }



  ngOnInit() {
    this.SendMobileOtp = false;
    this.SendEmailOtp = false;
    this.mobile_otp = 'none';
    this.email_otp = 'none';
    this.email_main_filed = 'none';
    this.noImage = document.location.origin + "/assets/img/uploads/upload.png";
    this.UserWalletInfo();
    this.UserReferalInfo();
    this.GetAllTickets();
    this.getUserProfileInfo();
    this.CheckAgentRequest();
    this.GetBankInfo();


  }

  getUserProfileInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/user-profile/get-user-info.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        if (result.user_details.fullname == '' || result.user_details.email == '') {
          this.ToggleEditProfile = 'block';
          this.ToggleShowProfile = 'none';
        }

        if (result.user_details.aadhaar_card_image == '') {
          this.aadhaar_card_image = this.noImage;
        } else {
          this.aadhaar_card_image = result.user_details.aadhaar_card_image;
        }
        if (result.user_details.pancard_image == '') {
          this.pancard_image = this.noImage;
        } else {
          this.pancard_image = result.user_details.pancard_image;
        }
        if (result.kycresponse.kycCode == 0) {
          this.KycFirstPage = 'none';
          this.documentUploadDiv = 'none';
          this.UploadUserBankDiv = 'none';
          this.CompleteKycDiv = 'block';
        }
        if (result.kycresponse.kycCode == 1) {
          this.KycFirstPage = 'block';
          this.documentUploadDiv = 'none';
          this.UploadUserBankDiv = 'none';
          this.CompleteKycDiv = 'none';
        }
        if (result.kycresponse.kycCode == 2) {
          this.KycFirstPage = 'none';
          this.documentUploadDiv = 'none';
          this.UploadUserBankDiv = 'block';
          this.CompleteKycDiv = 'none';
        }

        localStorage.setItem('fullname', result.user_details.fullname);
        this.fullname = result.user_details.fullname;
        this.wallet = result.user_details.wallet;
        this.mobile = result.user_details.mobile;
        this.email = result.user_details.email;
        this.user_profile = result.user_details.user_profile;
        this.aadhaar_card = result.user_details.aadhaar_card;
        this.pan_card = result.user_details.pan_card;

        this.paddress = result.user_details.paddress;
        this.pcity = result.user_details.pcity;
        this.pstate = result.user_details.pstate;
        this.pcode = result.user_details.pcode;
        this.caddress = result.user_details.caddress;
        this.ccity = result.user_details.ccity;
        this.cstate = result.user_details.cstate;
        this.ccode = result.user_details.ccode;
      }
    });
  }

  AddNewTicketForm() {
    this.http.post(this.ApiUrl + '/tickets/add-ticket.php', this.AddNewTicket.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.AddNewTicket.reset();
        this.AddNewTicketMsg = "Ticket Has Been Created Successfully";
        this.GetAllTickets();
        setTimeout(() => {
          this.AddNewTicketMsg = null;
        }, 3000);
      } else {
        this.AddNewTicketMsg = "Ticket Create Failed";
      }
    });
  }

  GetAllTickets() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/tickets/user-ticket-list.php', param).subscribe((result: any) => {
      this.TicketsList = result.tickets;
    });
  }





  UserReferalInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/referal-info/referal-info.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.referalData = result.user_data;
      }
    });
  }

  AddCommentFunc(ticket_id) {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'ticket_id': ticket_id };
    this.http.post(this.ApiUrl + '/tickets/ticket-info-by-id.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.AddCommentData.controls['ticket_id'].setValue(result.ticketArray.ticket_id);
        this.ticket_title = result.ticketArray.title;
        this.ticket_description = result.ticketArray.description;
        this.userComments = result.CommentArray;
        document.getElementById("openCommentBox").click();
      }
    });
  }

  GetUserCommentMessage(ticket_id) {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'ticket_id': ticket_id };
    this.http.post(this.ApiUrl + '/tickets/get-user-comment.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.userComments = result.CommentArray;
        this.AddCommentData.controls['comment'].setValue('');
      }
    });

  }


  AddCommentForm() {
    this.http.post(this.ApiUrl + '/tickets/add-user-comment.php', this.AddCommentData.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.GetUserCommentMessage(result.ticket_id);//
      }
    });
  }

  CompleteKycBtn() {
    this.KycFirstPage = 'none';
    this.documentUploadDiv = 'block';
  }

  uploadProfile(files: FileList, document_type: any) {
    this.fileToUpload = files.item(0);
    this.onUpload(document_type);

  }

  onUpload(document_type: any) {
    const uploadData = new FormData();
    uploadData.append('user_id', this.user_id);
    uploadData.append('auth_key', this.auth_key);
    uploadData.append('document_type', document_type);
    uploadData.append('documents', this.fileToUpload, this.fileToUpload.name);
    this.http.post(this.ApiUrl + '/user-profile/upload-documents.php', uploadData, { reportProgress: true, observe: 'events' }).subscribe((result: any) => {
      let response: any = result.body;
      if (response.error == 0) {
        this.getUserProfileInfo();
      }
    });
  }


  submitBankForm() {
    let data = this.AddBankForm.value;
    this.http.post(this.ApiUrl + "/bank-info/add-bank.php", this.AddBankForm.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.AddBankForm.reset();
        this.KycFirstPage = 'none';
        this.documentUploadDiv = 'none';
        this.UploadUserBankDiv = 'none';
        this.CompleteKycDiv = 'block';
        this.GetBankInfo();
      }
    });
  }

  UserWalletInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/user-profile/user-wallet-info.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.total_investment = result.total_investment;
      }
    });
  }

  BecameAnAgentFormSubmit() {
    let data = this.BecameAnAgent.value;
    this.http.post(this.ApiUrl + "/became-agent/became-an-agent.php", data).subscribe((result: any) => {
      if (result.error == 0) {
        this.BecameAnAgent.reset();
        this.CheckAgentRequest();
      } else {
        alert(result.msg);
      }
    });
  }

  CheckAgentRequest() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/became-agent/check-agent-request.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.AgentRequestMessage = "Your Request Status :" + result.status;
        this.AgentRequestMessageDiv = "block";
        this.AgentRequestFormDiv = "none";
      } else {
        this.AgentRequestMessageDiv = "none";
        this.AgentRequestFormDiv = "block";
      }
    });
  }

  get AgentfullnameField() {
    return this.BecameAnAgent.get('fullname');
  }

  get AgentAdditionMobileField() {
    return this.BecameAnAgent.get('additional_mobile_number');
  }


  sendOtp(type) {
    if (type == 'mobile') {
      let mobile_number = ((document.getElementById("additional_mobile_number") as HTMLInputElement).value);
      if (mobile_number.length != 10) {
        this.ErrorMobileNumber = "Invalid Mobile Number";
      } else {
        let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'method_type': type, 'method_data': mobile_number };
        this.http.post(this.ApiUrl + "/otp-request/send-otp.php", param).subscribe((result: any) => {
          if (result.error == 0) {
            this.mobile_main_filed = 'none';
            this.mobile_otp = 'block';
          } else {
            this.ErrorMobileNumber = "OTP SEND FAILED";
          }
        });
      }

    }

    if (type == 'email') {
      let email_address = ((document.getElementById("email_address_agent") as HTMLInputElement).value);
      let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'method_type': type, 'method_data': email_address };
      this.http.post(this.ApiUrl + "/otp-request/send-otp.php", param).subscribe((result: any) => {
        if (result.error == 0) {
          this.email_main_filed = 'none';
          this.email_otp = 'block';
        } else {
          this.ErrorEmailNumber = "OTP SEND FAILED";
        }
      });

    }
  }

  VerifyOtp(type) {
    if (type == 'mobile') {
      let mobile_number = ((document.getElementById("additional_mobile_number") as HTMLInputElement).value);
      let mobile_otp = ((document.getElementById("mobile_otp") as HTMLInputElement).value);
      let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'method_type': type, 'method_data': mobile_number, 'otp': mobile_otp };
      this.http.post(this.ApiUrl + "/otp-request/verify-otp.php", param).subscribe((result: any) => {
        if (result.error == 0) {
          this.mobile_main_filed = 'block';
          this.mobile_otp = 'none';
          this.email_main_filed = 'block';
          this.SendMobileOtp = true;
        } else {
          this.ErrorOtpMessage = "Wrong OTP";
        }
      });

    }
    if (type == 'email') {
      let email_address = ((document.getElementById("email_address_agent") as HTMLInputElement).value);
      let email_otp = ((document.getElementById("email_otp") as HTMLInputElement).value);
      let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'method_type': type, 'method_data': email_address, 'otp': email_otp };
      this.http.post(this.ApiUrl + "/otp-request/verify-otp.php", param).subscribe((result: any) => {
        if (result.error == 0) {
          this.email_main_filed = 'block';
          this.email_otp = 'none';
          this.SendEmailOtp = true;
        } else {
          this.ErrorEmailOtpMessage = "Wrong OTP";
        }
      });
      this.email_main_filed = 'block';
      this.email_otp = 'none';
    }
  }

  GetBankInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/bank-info/get-bank-info.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.bankrecords = result.records;
        this.bankdata = result.bankdata;
      } else {
        this.bankrecords = 0;
      }
    });
  }

  removeBank(bank_id: number) {
    var r = confirm('Do you Want to remove this bank from your list');
    let param = { 'bank_id': bank_id, 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    if (r == true) {
      this.http.post(this.ApiUrl + "/bank-info/delete-bank.php", param).subscribe((result: any) => {
        if (result.error == 0) {
          this.GetBankInfo();
        } else {
          alert(result.msg);
        }
      });
    }
  }

}