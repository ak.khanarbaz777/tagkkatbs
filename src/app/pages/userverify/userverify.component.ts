import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-userverify',
  templateUrl: './userverify.component.html',
  styleUrls: ['./userverify.component.scss']
})
export class UserverifyComponent implements OnInit {



  constructor(private http:HttpClient, private route:Router) { }
  ApiUrl = environment.baseUrl;
  user_id: any = parseInt(localStorage.getItem('user_id'));
  auth_key: string = localStorage.getItem('auth_key');
  errors:string;
  responseError:string;


  UserVerify = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    firstName: new FormControl('', [Validators.required,Validators.minLength(3)]),
    lastName: new FormControl('', [Validators.required,Validators.minLength(3)]),
    email: new FormControl('', [Validators.required]),
    referralCode: new FormControl(''),
    paddress: new FormControl('', [Validators.required]),
    pcity: new FormControl('', [Validators.required]),
    pstate: new FormControl('', [Validators.required]),
    pcode: new FormControl('', [Validators.required]),
    caddress: new FormControl(''),
    ccity: new FormControl(''),
    cstate: new FormControl(''),
    ccode: new FormControl('')
  });

  ngOnInit(): void {

  }

  get firstName(){
    this.errors = "Invalid First Name";
    return this.UserVerify.get('firstName');
  }
  get lastName(){
    this.errors = "Invalid Last Name";
    return this.UserVerify.get('lastName');
  }
  get email(){
    this.errors = "Invalid Email";
    return this.UserVerify.get('email');
  }

  get paddress(){
    this.errors = "Invalid Present Address";
    return this.UserVerify.get('paddress');
  }
  get pcity(){
    this.errors = "Invalid City";
    return this.UserVerify.get('pcity');
  }
  get pstate(){
    this.errors = "Invalid State";
    return this.UserVerify.get('pstate');
  }
  get pcode(){
    this.errors = "Invalid Postal Code";
    return this.UserVerify.get('pcode');
  }

  SubmitUserVerify() {
    console.log(this.UserVerify.value);
    this.http.post(this.ApiUrl + "/user-profile/user-kyc.php", this.UserVerify.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.route.navigate(['dashboard']);
      } else {
        this.responseError = result.msg;
        // alert(result.msg);
      }

    });
  }

}
