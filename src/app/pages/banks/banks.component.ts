import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.scss']
})
export class BanksComponent implements OnInit {
  

  constructor(private http: HttpClient, private router: Router) { }
  ApiUrl = environment.baseUrl;

  bankdata:Object;
  bankrecords:number;
  ErrorAccountNumber:string;

  ngOnInit(): void {
    this.GetBankInfo();
  }

  AddBankForm = new FormGroup({
    user_id: new FormControl(localStorage.getItem('user_id')),
    auth_key: new FormControl(localStorage.getItem('auth_key')),
    account_name: new FormControl('',[Validators.required,Validators.minLength(4)]),
    bank_name: new FormControl('',[Validators.required,Validators.minLength(4)]),
    account_number: new FormControl('',[Validators.required,Validators.minLength(8)]),
    re_account_number: new FormControl('',[Validators.required,Validators.minLength(8)]),
    ifsc_code: new FormControl('',[Validators.required,Validators.minLength(6)]),
    upi: new FormControl('',[Validators.required])

  },

  );

  get GetAccountName(){
    return this.AddBankForm.get('account_name');
    // console.log(this.AddBankForm.get('account_name'));
  }
  
  get GetBank_name(){
    return this.AddBankForm.get('bank_name');
  }
  get GetAccount_number(){
    return this.AddBankForm.get('account_number');
  }

  

  get GetRe_account_number(){
    return this.AddBankForm.get('re_account_number');
  }
  get GetIfsc_code(){
    return this.AddBankForm.get('ifsc_code');
  }

  

  

  GetBankInfo(){
    let param = {'user_id':localStorage.getItem('user_id'),'auth_key':localStorage.getItem('auth_key')};
    this.http.post(this.ApiUrl+"/bank-info/get-bank-info.php",param).subscribe((result:any) => {
      if(result.error == 0){
        this.bankrecords = result.records;
        this.bankdata = result.bankdata;
      }else{
        this.bankrecords = 0;

      }
      console.log(result);
    });

  }

  submitBankForm(){

    let data = this.AddBankForm.value;
    if(data.account_number != data.re_account_number){
      this.ErrorAccountNumber = 'Both Account Number Must be Same';
    }else{
      this.ErrorAccountNumber = '';
    this.http.post(this.ApiUrl+"/bank-info/add-bank.php",this.AddBankForm.value).subscribe((result:any) => {
      if(result.error == 0){
        this.GetBankInfo();
        this.AddBankForm.reset();

      }else{
        this.ErrorAccountNumber = result.msg;
        // alert(result.msg);
      }

    });
    // 
    }
  }

  removeBank(bank_id:number){
    var r = confirm('Do you Want to remove this bank from your list');
    let param = {'bank_id':bank_id,'user_id':localStorage.getItem('user_id'),'auth_key':localStorage.getItem('auth_key')};

    if(r==true){
      this.http.post(this.ApiUrl+"/bank-info/delete-bank.php",param).subscribe((result:any)=>{
        console.log(result);
        if(result.error == 0){
          this.GetBankInfo();
          // alert("delete Successf")
        }else{
          alert(result.msg);
        }


      });
      
      // 
    }
    // console.log("do you want")
  }

}
