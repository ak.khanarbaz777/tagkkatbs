import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }
  ApiUrl = environment.baseUrl;
  title = 'Project Info title';
  project_id = this.route.snapshot.paramMap.get('project_id');
  project_obj: any;

  ngOnInit(): void {
    this.GetProjectDetail();
  }

  GetProjectDetail() {
    let param = { 'project_id': this.project_id };
    this.http.post(this.ApiUrl + "/projects/project-detail.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.project_obj = result.project;
      }
    });
  }

}
