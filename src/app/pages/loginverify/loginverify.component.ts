import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-loginverify',
  templateUrl: './loginverify.component.html',
  styleUrls: ['./loginverify.component.scss']
})
export class LoginverifyComponent implements OnInit, OnDestroy {
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }
  ApiUrl = environment.baseUrl;
  mobilepara = this.route.snapshot.paramMap.get('mobile');
  ngOnInit() {
    let user_id = localStorage.getItem('user_id');
    let auth_key = localStorage.getItem('auth_key');
    this.startTimer();
    
  }
  ngOnDestroy() {
  }
  mobile: any = this.route.snapshot.paramMap.get('mobile');

  loginUsersVerify = new FormGroup({
    mobile: new FormControl(this.mobile),
    otp: new FormControl('')
  })

  loginUserSubmit() {
    // console.log(this.loginUsersVerify.value);
    this.http.post(this.ApiUrl + '/auth/loginverify.php', this.loginUsersVerify.value).subscribe((result: any) => {
      console.log(result);
      if (result.error == 0) {
        localStorage.setItem("user_id", result.user_id);
        localStorage.setItem("auth_key", result.auth_key);
        localStorage.setItem("fullname", result.fullname);
        localStorage.setItem("mobile", result.mobile);
        if(result.is_verify != 'yes'){
          this.router.navigate(['/user-verify']);
        }else{
          this.router.navigate(['/dashboard']);
        }
        
      } else {
        alert(result.msg);
        console.log(result.msg)
      }
    });
  }

  timeLeft: number = 59;
  interval;
  ResendOTPBtn = true;

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.ResendOTPBtn = false;
      }
    },1000)
  }

  ResendOTP(){
    let requestparam = {'mobile':this.mobilepara};
    this.http.post(this.ApiUrl + '/auth/loginuser.php', requestparam).subscribe((result: any) => {
      if (result.error == 0) {
        alert("Otp Resend Successfully");
      } else {
        alert(result.msg);
        // console.log(result.msg)
      }
    });
  }

}
