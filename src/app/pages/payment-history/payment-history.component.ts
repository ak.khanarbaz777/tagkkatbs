import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss']
})
export class PaymentHistoryComponent implements OnInit {

  constructor(private http: HttpClient) { }
  ApiUrl = environment.baseUrl;
  title = 'Payment History';
  totalPayments: any;
  value:number = 0;
  resultRecord:number = 0;
  disabledPreviousClass = 'disabled';
  disabledNextClass = 'disabled';

  param:Object = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

  ngOnInit(): void {
    this.GetPaymentHistory();
  }
  GetPaymentHistory(){
    this.http.post(this.ApiUrl + "/payments/payment-history.php", this.param).subscribe((result: any) => {
      if (result.error == 0) {
        this.resultRecord = result.totalrecords;
        this.totalPayments = result.records;
      } else {
        this.resultRecord = 0;
      }
    });

  }


  paginationList(event) {
    if(event == 'previous'){
      this.value -= 20; 
    }
    if(event == 'next'){
      this.value += 20;
    }
    if(this.value <= 0){
      this.disabledPreviousClass = 'disabled';
    }else{
      this.disabledPreviousClass = '';
    }
    this.param['offset'] = this.value;
    this.http.post(this.ApiUrl + "/payments/payment-history.php", this.param).subscribe((result: any) => {
      console.log(result);
      if (result.error == 0) {
        this.resultRecord = result.totalrecords;
        this.totalPayments = result.records;
        if(result.totalrecords == 0){
          this.disabledNextClass = 'disabled';
        }else{
          this.disabledNextClass = '';
        }
      } else {
        this.resultRecord = 0;
      }
    });
  }

  


}
