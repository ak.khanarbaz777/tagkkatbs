import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent implements OnInit {

  constructor(private http: HttpClient) { }
  ApiUrl = environment.baseUrl;
  bankrecords: number;
  bankdata: Object;

  withdrawalRequestRecord: number;
  withdrawalRequestData: Object;

  ErrorMsg: string;

  ngOnInit(): void {
    this.GetBankInfo();
    this.GetWithdrawalRequest();
  }

  GetBankInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/bank-info/get-bank-info.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.bankrecords = result.records;
        this.bankdata = result.bankdata;
      } else {
        this.bankrecords = 0;

      }
    });

  }

  GetWithdrawalRequest() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/withdrawl-request/get-withdrawal-request.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.withdrawalRequestRecord = result.records;
        this.withdrawalRequestData = result.requestdata;
      } else {
        this.withdrawalRequestRecord = 0;
      }
    });

  }

  WithdrawlRequestForm = new FormGroup({
    user_id: new FormControl(localStorage.getItem('user_id')),
    auth_key: new FormControl(localStorage.getItem('auth_key')),
    bank_id: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required])
  });


  SubmitWithdrawlRequest() {
    this.http.post(this.ApiUrl + "/withdrawl-request/add-withdrawl-request.php", this.WithdrawlRequestForm.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.GetWithdrawalRequest();
        this.WithdrawlRequestForm.reset();
      } else {
        this.ErrorMsg = result.msg;
        setTimeout(() => {
          this.ErrorMsg = null;
        }, 3000);
      }
    });
  }
}
