import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { PaymentService } from './payment.service';
// import { environment } from '../../../environments/environment';
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  ApiUrl = environment.baseUrl;
  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  resultRecord: number;
  totalPayments: Object;
  withdrawalRequestRecord: number;
  withdrawalRequestData: Object;

  totalInvestment: number;
  totalTransetionNum: string;
  projects: any;
  totalInterest: number;
  constructor(public payment: PaymentService, private http: HttpClient, private router: Router) { }


  ngOnInit() {
    this.TotalTransection();
    this.TotalInvestment();
    this.RecentPayments();
    this.RecentWithdrawal();
    this.TotalInterest();
    this.ProjectLists();

    this.datasets = [
      [0, 20, 10, 30, 15, 40, 20, 60, 60],
      [0, 20, 5, 25, 10, 30, 15, 40, 40]
    ];
    this.data = this.datasets[0];


    var chartOrders = document.getElementById('chart-orders');

    parseOptions(Chart, chartOptions());


    var ordersChart = new Chart(chartOrders, {
      type: 'bar',
      options: chartExample2.options,
      data: chartExample2.data
    });

    var chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
      type: 'line',
      options: chartExample1.options,
      data: chartExample1.data
    });
  }


  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.data;
    this.salesChart.update();
  }

  ProjectLists() {
    this.http.get(this.ApiUrl + '/projects/project-lists.php').subscribe((result: any) => {
      if (result.error == 0) {
        this.projects = result.projects;
      }
    });
  }





  rzp1;
  amount: number = 5000;

  paymentSuccess(data: Object) {
    console.log(data);
    this.http.post(this.ApiUrl + '/payments/payment-verify.php', data).subscribe((result: any) => {
      if (result.error == 0) {
        this.router.navigate(['/payment-history']);
      } else {
        alert(result.msg);
      }
    });
  }


  generateOrderId(result: any) {
    console.log(result);
    let options = {
      "key": result.key_id,
      "amount": result.amount,
      "currency": "INR",
      "name": "Invest Now",
      "description": "Invest now to tagkka",
      "image": "http://localhost:4200/assets/img/brand/red.png",
      "order_id": result.order_id,
      "handler": function (response: any) {
        let mydata = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'razorpay_payment_id': response.razorpay_payment_id, 'razorpay_order_id': response.razorpay_order_id, 'razorpay_signature': response.razorpay_signature, 'amount': this.amount,'pay_id':result.pay_id };
        this.paymentSuccess(mydata);
      }.bind(this),
      "prefill": {
        "name": result.fullname,
        "email": result.email,
        "contact": result.mobile
      },
      "notes": {
        "address": "Razorpay Corporate Office"
      },
      "theme": {
        "color": "#3399cc"
      }
    };
    return options;

  }

  PayBtn() {
    let amount = this.amount * 100;
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'), 'amount': amount };

    this.http.post(this.ApiUrl + '/payments/generate-order.php', param).subscribe((result: Object) => {
      let options = this.generateOrderId(result);
      this.rzp1 = new this.payment.nativeWindow.Razorpay(options);
      this.rzp1.open();
    })


  }


  TotalInvestment() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/payments/total-investment.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.totalInvestment = result.totalInvestment;
      } else {
        this.totalInvestment = 0;
      }
    });
  }

  TotalInterest() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + '/bank-info/get-interest.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.totalInterest = result.totalinterest;
      } else {
        this.totalInterest = 0;
      }
    });
  }

  TotalTransection() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + '/payments/transection-number.php', param).subscribe((result: any) => {
      if (result.error == 0) {
        this.totalTransetionNum = result.totaltransection;
      } else {
        this.totalTransetionNum = "0";
      }
    });
  }

  RecentPayments() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + "/payments/recent-payments.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.resultRecord = result.totalrecords;
        this.totalPayments = result.records;
      } else {
        this.resultRecord = 0;
      }
    });
  }

  RecentWithdrawal() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + "/withdrawl-request/recent-withdrawal-request.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.withdrawalRequestRecord = result.records;
        this.withdrawalRequestData = result.requestdata;
      } else {
        this.withdrawalRequestRecord = 0;
      }
    });
  }

  amountError: string;
  isDisable: boolean = false;

  GetAmountValue(event: any) {
    this.amount = event.target.value;
    console.log(this.amount);
    if (this.amount >= 5000) {
      this.isDisable = false;
      this.amountError = '';
    } else {
      this.isDisable = true;
      this.amountError = "Amount Must be Greater than 5000";
    }
  }
}
