import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-business-profile',
  templateUrl: './business-profile.component.html',
  styleUrls: ['./business-profile.component.scss']
})
export class BusinessProfileComponent implements OnInit {

  constructor(private http: HttpClient) { }
  ApiUrl = environment.baseUrl;
  occupation: string;
  business_type: string;
  GST: string;
  TIN: string;
  business_mobile: string;
  business_email: string;
  business_address: string;

  ErrorMsg: string;

  editBusinessProfile: string = 'block';
  showBusinessProfile: string = 'none';


  businessForm = new FormGroup({
    user_id: new FormControl(localStorage.getItem('user_id')),
    auth_key: new FormControl(localStorage.getItem('auth_key')),
    occupation: new FormControl('', [Validators.required, Validators.minLength(4)]),
    business_type: new FormControl('', [Validators.required]),
    GST: new FormControl('', [Validators.required, Validators.minLength(4)]),
    TIN: new FormControl('', [Validators.required, Validators.minLength(4)]),
    business_mobile: new FormControl('', [Validators.required, Validators.minLength(10)]),
    business_email: new FormControl('', [Validators.required, Validators.minLength(5)]),
    business_address: new FormControl('', [Validators.required]),

  });

  ngOnInit(): void {
    this.GetBusinessInfo();


  }

  get occupationField() {
    return this.businessForm.get('occupation');
  }
  get GSTField() {
    return this.businessForm.get('GST');
  }
  get TINField() {
    return this.businessForm.get('TIN');
  }
  get business_mobileField() {
    return this.businessForm.get('business_mobile');
  }
  get business_emailField() {
    return this.businessForm.get('business_email');
  }
  get business_addressField() {
    return this.businessForm.get('business_address');
  }

  GetBusinessInfo() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/user-profile/get-business-profile.php", param).subscribe((result: any) => {
      console.log(result);
      if (result.error == 0) {
        if (result.data.occupation != '' || result.data.occupation != null) {
          this.editBusinessProfile = 'none';
          this.showBusinessProfile = 'block';
        }
        this.businessForm.setValue({
          user_id: localStorage.getItem('user_id'),
          auth_key: localStorage.getItem('auth_key'),
          occupation: result.data.occupation,
          business_type: result.data.business_type,
          GST: result.data.GST,
          TIN: result.data.TIN,
          business_mobile: result.data.business_mobile,
          business_email: result.data.business_email,
          business_address: result.data.business_address,

        });
        this.occupation = result.data.occupation;
        this.business_type = result.data.business_type;
        this.GST = result.data.GST;
        this.TIN = result.data.TIN;
        this.business_mobile = result.data.business_mobile;
        this.business_email = result.data.business_email;
        this.business_address = result.data.business_address;


      } else {
        // alert(result.msg);
        console.log("get value error");
      }
      // alert(result.msg);
    });
  }

  submitBusinessForm() {

    this.http.post(this.ApiUrl + "/user-profile/update-business-profile.php", this.businessForm.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.GetBusinessInfo();
        this.editBusinessProfile = 'none';
        this.showBusinessProfile = 'block';
        // console.log(result);
        this.ErrorMsg = "Successfully Updated";

        //
      } else {
        this.ErrorMsg = result.msg;
        // 
      }
      // alert(result.msg);
    });


  }
  OpenEditBusiness() {
    this.editBusinessProfile = 'block';
    this.showBusinessProfile = 'none';
  }

  options = {
    types: [],
    componentRestrictions: { country: 'IN' }
  };


  handleAddressChange(address) {
    this.business_address = address.formatted_address;
    console.log(this.business_address);
  }

}
