import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  ApiUrl = environment.baseUrl;
  public aFormGroup: FormGroup;
  constructor(private http: HttpClient, private router: Router,private formBuilder: FormBuilder) { }
  siteKey:string;
  public Compact = 'Normal';
  ngOnInit() {
    // let user_id = localStorage.getItem('user_id');
    // let auth_key = localStorage.getItem('auth_key');
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required],
      mobile: ['', Validators.required]
    });
    this.siteKey = "6LfUURAdAAAAAJw2Yn4qRVuH4WOMzp9D28cSXA0z";
  }
  ngOnDestroy() {
  }

  // loginUsers = new FormGroup({
  //   mobile: new FormControl('', [Validators.required, Validators.minLength(9)])
  // });
 get mobile(){
   console.log(this.aFormGroup.get('mobile'));
   return this.aFormGroup.get('mobile');
 }
 

  loginUserSubmit() {
    this.http.post(this.ApiUrl + '/auth/loginuser.php', this.aFormGroup.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.router.navigate(['/loginverify', result.mobile]);
      } else {
        alert(result.msg);
      }
    });
  }

  
}
