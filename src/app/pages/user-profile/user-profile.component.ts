import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavbarComponent } from './../../components/navbar/navbar.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }
  ApiUrl = environment.baseUrl;
  user_id: any = parseInt(localStorage.getItem('user_id'));
  auth_key: string = localStorage.getItem('auth_key');
  fullname: string;
  wallet: string;
  mobile: string;
  email: string;
  aadhaar_card: string;
  pan_card: string;
  paddress: string;
  pcity: string;
  pstate: string;
  pcode: string;
  caddress: string;
  ccity: string;
  cstate: string;
  ccode: string;
  user_profile: string;

  ErrorMsg: string;
  ToggleEditProfile: string = 'none';
  ToggleShowProfile: string = 'block';

  fileToUpload: File | null = null;
  // UserProfileForm: FormGroup;
  UserProfileForm = new FormGroup({
    user_id: new FormControl(this.user_id),
    auth_key: new FormControl(this.auth_key),
    action: new FormControl('action'),
    fullname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    aadhaar_card: new FormControl('', [Validators.required, Validators.minLength(12), Validators.maxLength(12), Validators.pattern("^[0-9]*$")]),
    pan_card: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    paddress: new FormControl('', [Validators.required]),
    pcity: new FormControl('', [Validators.required]),
    pstate: new FormControl('', [Validators.required]),
    pcode: new FormControl('', [Validators.required]),
    caddress: new FormControl('', [Validators.required]),
    ccity: new FormControl('', [Validators.required]),
    cstate: new FormControl('', [Validators.required]),
    ccode: new FormControl('', [Validators.required])
  });




  ngOnInit() {


    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + '/user-profile/get-user-info.php', param).subscribe((result: any) => {
      if (result.user_details.fullname == '' || result.user_details.email == '') {
        this.ToggleEditProfile = 'block';
        this.ToggleShowProfile = 'none';
      }
      localStorage.setItem('fullname', result.user_details.fullname);
      this.fullname = result.user_details.fullname;
      this.wallet = result.user_details.wallet;
      this.mobile = result.user_details.mobile;
      this.email = result.user_details.email;
      this.user_profile = result.user_details.user_profile;
      this.aadhaar_card = result.user_details.aadhaar_card;
      this.pan_card = result.user_details.pan_card;
      this.paddress = result.user_details.paddress;
      this.pcity = result.user_details.pcity;
      this.pstate = result.user_details.pstate;
      this.pcode = result.user_details.pcode;
      this.caddress = result.user_details.caddress;
      this.ccity = result.user_details.ccity;
      this.cstate = result.user_details.cstate;
      this.ccode = result.user_details.ccode;
      this.UserProfileForm.setValue({
        user_id: this.user_id,
        auth_key: this.auth_key,
        action: 'action',
        fullname: result.user_details.fullname,
        email: result.user_details.email,
        aadhaar_card: result.user_details.aadhaar_card,
        pan_card: result.user_details.pan_card,
        paddress: result.user_details.paddress,
        pcity: result.user_details.pcity,
        pstate: result.user_details.pstate,
        pcode: result.user_details.pcode,
        caddress: result.user_details.caddress,
        ccity: result.user_details.ccity,
        cstate: result.user_details.cstate,
        ccode: result.user_details.ccode
      });



    });
  }



  get fullnameField() {
    return this.UserProfileForm.get('fullname');
  }

  get emailField() {
    return this.UserProfileForm.get('email');
  }
  get aadhaar_cardField() {
    return this.UserProfileForm.get('aadhaar_card');
  }
  get pan_cardField() {
    return this.UserProfileForm.get('pan_card');
  }
  get paddressField() {
    return this.UserProfileForm.get('paddress');
  }
  get pcityField() {
    return this.UserProfileForm.get('pcity');
  }
  get pstateField() {
    return this.UserProfileForm.get('pstate');
  }
  get pcodeField() {
    return this.UserProfileForm.get('pcode');
  }
  get caddressField() {
    return this.UserProfileForm.get('caddress');
  }
  get ccityField() {
    return this.UserProfileForm.get('ccity');
  }
  get cstateField() {
    return this.UserProfileForm.get('cstate');
  }
  get ccodeField() {
    return this.UserProfileForm.get('ccode');
  }

  showProfileInfo() {
    this.ToggleEditProfile = 'block';
    this.ToggleShowProfile = 'none';
  }



  UserProfileSubmit() {
    this.http.post(this.ApiUrl + "/user-profile/edit-profile.php", this.UserProfileForm.value).subscribe((result: any) => {
      if (result.error == 0) {
        this.ToggleEditProfile = 'none';
        this.ToggleShowProfile = 'block';
      } else {
        this.ErrorMsg = result.msg;
        // alert(result.msg)
      }

    });

  }
  uploadProfile(files: FileList) {
    this.fileToUpload = files.item(0);
    this.onUpload();

  }

  onUpload() {
    const uploadData = new FormData();
    uploadData.append('user_id', this.user_id);
    uploadData.append('auth_key', this.auth_key);
    uploadData.append('profilepicture', this.fileToUpload, this.fileToUpload.name);
    this.http.post(this.ApiUrl + '/user-profile/upload-profile-photo.php', uploadData, { reportProgress: true, observe: 'events' }).subscribe((result: any) => {
      let response: any = result.body;
      if (response.error == 0) {
        // this.user_profile = response.user_profile;
        window.location.reload();
      } else {
        this.ErrorMsg = response.msg;
      }
    });
  }
}

