import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient) { }

  ApiUrl = environment.baseUrl;

  fullname: string;
  user_profile: string;
  notifications: any;
  unseen_notification: number;

  ngOnInit(): void {
    this.UpdateProfilePhoto();
    this.GetNotification();
  }

  UpdateProfilePhoto() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/user-profile/get-user-info.php", param).subscribe((result: any) => {
      this.fullname = result.user_details.fullname;
      this.user_profile = result.user_details.user_profile;
    });
  }

  GetNotification() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };
    this.http.post(this.ApiUrl + "/notifications/user-notifications.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.notifications = result.notificatons;
        this.unseen_notification = result.unseen_notification;
      }
    });
  }

  SeenNotification(notification_id){
    console.log(notification_id);
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key'),'notification_id':notification_id };
    this.http.post(this.ApiUrl + "/notifications/read-notification-api.php", param).subscribe((result: any) => {
      if (result.error == 0) {
        this.GetNotification();
      }
    });
    

  }

  logOutUser() {
    localStorage.removeItem("user_id");
    localStorage.removeItem("auth_key");
    localStorage.removeItem("mobile");
    this.router.navigate(['/login']);
  }

}
