import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/user-profile2', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
  { path: '/business-profile', title: 'Business Profile', icon: 'ni-building text-primary', class: '' },
  { path: '/payment-history', title: 'Payment History', icon: 'ni-chart-bar-32 text-red', class: '' },
  // { path: '/bank-info', title: 'Bank Infomations', icon: 'ni-money-coins text-info', class: '' },
  { path: '/withdrawal', title: 'Withdraw Amount', icon: 'fas fa-money-bill-wave text-success', class: '' },
  { path: '/projects', title: 'Projects', icon: 'fas fa-file text-success', class: '' },



];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  ApiUrl = environment.baseUrl;
  public menuItems: any[];
  public isCollapsed = true;
  fullname: string;
  user_profile: string;
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.UpdateProfilePhoto();
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }

  UpdateProfilePhoto() {
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + "/user-profile/get-user-info.php", param).subscribe((result: any) => {
      this.fullname = result.user_details.fullname;
      this.user_profile = result.user_details.user_profile;


    });
  }

  logOutUser() {
    localStorage.removeItem("user_id");
    localStorage.removeItem("auth_key");
    localStorage.removeItem("mobile");
    this.router.navigate(['/login']);
  }
}
