import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  ApiUrl = environment.baseUrl;
  public focus;
  public listTitles: any[];
  public location: Location;
  constructor(location: Location, private element: ElementRef, private router: Router,private http:HttpClient) {
    this.location = location;
  }

  fullname:string;
  user_profile:string;

  ngOnInit() {

    this.UpdateProfilePhoto();
    
    this.listTitles = ROUTES.filter(listTitle => listTitle);

  }

  UpdateProfilePhoto(){
    let param = { 'user_id': localStorage.getItem('user_id'), 'auth_key': localStorage.getItem('auth_key') };

    this.http.post(this.ApiUrl + "/user-profile/get-user-info.php",param).subscribe((result:any)=>{
      this.fullname = result.user_details.fullname;
      this.user_profile = result.user_details.user_profile;
      
      
    });
  }
  getTitle() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === titlee) {
        return this.listTitles[item].title;
      }
    }
    return 'Dashboard';
  }

  logOutUser() {
    localStorage.removeItem("user_id");
    localStorage.removeItem("auth_key");
    localStorage.removeItem("mobile");
    this.router.navigate(['/login']);
  }

}
