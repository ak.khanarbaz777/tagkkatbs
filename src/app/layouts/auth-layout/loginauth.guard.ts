import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginauthGuard implements CanActivate {
  constructor(private router:Router){}
  canActivate(){
    let user_id = localStorage.getItem("user_id");
    // console.log("user id "+user_id);
    // console.log("auth_key id "+auth_key);
    if(user_id != '' || user_id != null){
      this.router.navigate(['/']);
      return true; 
    }
    return false;
    
  }
  
}
