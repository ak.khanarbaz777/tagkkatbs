import { Routes } from '@angular/router';

import { LoginComponent } from '../../pages/login/login.component';
import { LoginverifyComponent } from '../../pages/loginverify/loginverify.component';
import { LoginauthGuard } from './loginauth.guard';
import { UserverifyComponent } from '../../pages/userverify/userverify.component';
export const AuthLayoutRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'loginverify/:mobile', component: LoginverifyComponent },
    { path: 'user-verify', component: UserverifyComponent }
];
