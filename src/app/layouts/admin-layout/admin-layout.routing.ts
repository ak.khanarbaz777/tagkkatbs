import { Routes } from '@angular/router';
import { BanksComponent } from 'src/app/pages/banks/banks.component';
import { PaymentHistoryComponent } from 'src/app/pages/payment-history/payment-history.component';
import { WithdrawalComponent } from 'src/app/pages/withdrawal/withdrawal.component';
import { BusinessProfileComponent } from 'src/app/pages/business-profile/business-profile.component';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { UserProfile2Component  } from '../../pages/user-profile2/user-profile2.component';
import { AuthGuardGuard } from './auth-guard.guard';
import { ProjectsComponent } from '../../pages/projects/projects.component';
import { ProjectDetailComponent } from 'src/app/pages/project-detail/project-detail.component';
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent, canActivate:[AuthGuardGuard] },
    { path: 'user-profile',   component: UserProfileComponent, canActivate:[AuthGuardGuard] },
    { path: 'payment-history',   component: PaymentHistoryComponent, canActivate:[AuthGuardGuard] },
    { path: 'withdrawal',   component: WithdrawalComponent, canActivate:[AuthGuardGuard] },
    { path: 'bank-info',   component: BanksComponent, canActivate:[AuthGuardGuard] },
    { path: 'business-profile',   component: BusinessProfileComponent, canActivate:[AuthGuardGuard] },
    { path: 'projects',   component: ProjectsComponent, canActivate:[AuthGuardGuard] },
    { path: 'user-profile2',   component: UserProfile2Component, canActivate:[AuthGuardGuard] },
    { path: 'project-detail/:project_id',   component: ProjectDetailComponent, canActivate:[AuthGuardGuard] }

];
