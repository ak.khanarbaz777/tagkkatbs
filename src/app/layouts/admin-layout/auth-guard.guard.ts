import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private router:Router) {}
  canActivate(){
    let user_id = localStorage.getItem("user_id");
    let auth_key = localStorage.getItem("auth_key");
    console.log("user id "+user_id);
    console.log("auth_key id "+auth_key);
    if(user_id != '' && auth_key != '' && auth_key != null && user_id != null){
      return true;
    }else{
      this.router.navigate(['/login']);
    }
    return false; 
  }
  
}
